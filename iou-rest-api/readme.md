# IOU REST API

## Endpoints

* GET /users - Get a list of user information 
```curl
http://localhost:9006/users?users=James&users=Bob"
```

* POST /user  - Create a new user
```json
{
   "name":"Bob"
}
```

* POST /iou - Add a new IOU entry
```json
{
  "amount": 25,
  "borrower": "Zack",
  "lender": "Bob"
}
```

* Swagger Endpoint - /swagger-ui.html
```
http://localhost:9006/swagger-ui.html
```

### Development Stack
* Java 8
* Maven 3.6.3
* Spring Boot 2.3.3.RELEASE

### Build Project 
mvn clean package

### Run Tests
mvn clean test

### Nice to have
* Aspect Logging


