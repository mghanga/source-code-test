package com.iourest.controllers;

import com.iourest.service.IUsersService;
import com.iourest.vm.IouVm;
import com.iourest.vm.UserVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Optional;

@RestController
public class UsersController {

    @Autowired private IUsersService iUsersService;

    @GetMapping("/users")
    public ResponseEntity<?> addNewUser(@RequestParam @NotEmpty List<String> users){
        return ResponseEntity.of(Optional.of(iUsersService.getUsersByNames( users ) ));
    }

    @PostMapping("/add")
    public ResponseEntity<?> addNewUser(@RequestBody @Valid UserVm userVm){
        return ResponseEntity.of(Optional.of(iUsersService.createUser( userVm ) ));
    }

    @PostMapping("/iou")
    public ResponseEntity<?> createIouEntry(@RequestBody @Valid IouVm iouVm){
        return ResponseEntity.of(Optional.of(iUsersService.createIouEntry( iouVm ) ));
    }
}
