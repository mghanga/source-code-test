package com.iourest.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.iourest.entities.Entries;
import com.iourest.entities.Users;
import com.iourest.repository.EntriesRepository;
import com.iourest.repository.UsersRepository;
import com.iourest.vm.IouVm;
import com.iourest.vm.UserVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class UsersService implements IUsersService {

    @Autowired private UsersRepository usersRepository;
    @Autowired private EntriesRepository entriesRepository;


    /**
     * Get all users
     *
     * @return {@link List<Users> }
     */
    @Override
    public Object getUsersByNames(List<String> names) {
        return  parseUsersListResponse(usersRepository.findAllByNameInOrderByName( names ));
    }

    /**
     * Create a new user
     *
     * @param {@link UserVm} userVm
     * @return {@link com.fasterxml.jackson.databind.JsonNode}
     */
    public Object createUser(UserVm userVm){
        usersRepository.findByName( userVm.getName() ).ifPresent( e -> { throw new ResponseStatusException( HttpStatus.BAD_REQUEST, "User already exists"); });

        Users user = new Users();
        user.setName( userVm.getName() );
        usersRepository.save( user );
        return parseUserResponse( user );
    }

    /**
     * Create a new IOU entry
     *
     * @param iouVm {@link IouVm}
     */
    public Object createIouEntry(IouVm iouVm){
        Users lender = usersRepository.findByName( iouVm.getLender() ).orElseThrow( () -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "The lender is unknown." ));
        Users borrower = usersRepository.findByName( iouVm.getBorrower() ).orElseThrow( () -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "The borrower is unknown." ) );

        BigDecimal amount = iouVm.getAmount();

        Entries entry = new Entries();
        entry.setAmount( amount );
        entry.setLender( lender );
        entry.setBorrower( borrower );
        entriesRepository.save( entry );

        // Lending increases one's balance
        BigDecimal lenderBalance = lender.getBalance().add( amount );
        lender.setBalance( lenderBalance );
        usersRepository.save( lender );

        // Borrowing reduces one's balance
        BigDecimal borrowerBalance = borrower.getBalance().subtract( amount );
        borrower.setBalance( borrowerBalance );
        usersRepository.save( borrower );

        return parseUsersListResponse(usersRepository.findAllByOrderByName());
    }

    private Object parseUsersListResponse(Optional<List<Users>> users){
        if( users.isPresent() ){
            List<Users> usersList = users.get();
            return usersList.stream().map(this::parseUserResponse).collect(Collectors.toList());
        }
        else{
            return Collections.EMPTY_LIST;
        }
    }


    private Object parseUserResponse(Users user){
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode rootNode = mapper.createObjectNode();
        rootNode.put("name", user.getName() );
        rootNode.put("balance", user.getBalance() );

        ObjectNode owes = mapper.createObjectNode();
        String name;
        BigDecimal total;
        for( Entries entry : user.getOwes()){
            name = entry.getLender().getName();

            if( owes.has( name ) ){
                total = owes.get( name ).decimalValue().add( entry.getAmount()  );
                owes.put(name, total );
            }
            else owes.put(name, entry.getAmount() );
        }

        ObjectNode owedBy = mapper.createObjectNode();
        for( Entries entry : user.getOwedBy()){
            name = entry.getBorrower().getName();
            if( owedBy.has( name ) ){
                total = owedBy.get( name ).decimalValue().add( entry.getAmount()  );
                owedBy.put(name, total );
            }
            else  owedBy.put(name, entry.getAmount() );
        }

        rootNode.set("owes", owes );
        rootNode.set("owed_by", owedBy );
        return rootNode;
    }


}
