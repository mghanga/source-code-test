package com.iourest.service;

import com.iourest.vm.IouVm;
import com.iourest.vm.UserVm;

import java.util.List;

public interface IUsersService {

    Object getUsersByNames(List<String> names);
    Object createUser(UserVm userVm);
    Object createIouEntry(IouVm iouVm);


}
