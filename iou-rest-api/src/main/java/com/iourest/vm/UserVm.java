package com.iourest.vm;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class UserVm {

    @Size( min = 1, max = 100)
    private String name;
}
