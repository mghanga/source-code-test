package com.iourest.vm;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
public class IouVm {

    @NotNull
    private String lender;

    @NotNull
    private String borrower;

    private BigDecimal amount;
}
