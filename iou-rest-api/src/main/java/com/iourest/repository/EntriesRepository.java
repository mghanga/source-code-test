package com.iourest.repository;

import com.iourest.entities.Entries;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EntriesRepository extends CrudRepository<Entries, Long> {
}
