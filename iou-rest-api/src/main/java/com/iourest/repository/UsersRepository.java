package com.iourest.repository;

import com.iourest.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long> {

    Optional<List<Users>> findAllByOrderByName();
    Optional<List<Users>> findAllByNameInOrderByName(List<String> names);
    Optional<Users> findByName(String name);
}
