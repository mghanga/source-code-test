package com.iourest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IouRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(IouRestApplication.class, args);
    }

}
