//package com.iourest.entities;
//
//import com.fasterxml.jackson.annotation.JsonBackReference;
//import lombok.Data;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.math.BigDecimal;
//
//
//@Data
//@Entity(name = "Owe")
//@Table(name = "owes")
//public class Owe implements Serializable {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    private BigDecimal amount;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "user_id")
//    @JsonBackReference
//    private Users user;
//
//    @Override
//    public boolean equals(Object current) {
//        if (this == current) return true;
//        if (!(current instanceof Owe)) return false;
//        return id != null && id.equals(((Owe) current).getId());
//    }
//
//    @Override
//    public int hashCode() {
//        return 31;
//    }
//}
