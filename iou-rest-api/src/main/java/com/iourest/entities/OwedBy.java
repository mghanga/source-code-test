//package com.iourest.entities;
//
//import com.fasterxml.jackson.annotation.JsonBackReference;
//import lombok.Data;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.math.BigDecimal;
//
//
//@Data
//@Entity(name = "Owed By")
//@Table(name = "owed_by")
//public class OwedBy implements Serializable {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    private BigDecimal amount;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "user_id")
//    @JsonBackReference
//    private Users user;
//
//    @Override
//    public boolean equals(Object current) {
//        if (this == current) return true;
//        if (!(current instanceof OwedBy)) return false;
//        return id != null && id.equals(((OwedBy) current).getId());
//    }
//
//    @Override
//    public int hashCode() {
//        return 31;
//    }
//}
