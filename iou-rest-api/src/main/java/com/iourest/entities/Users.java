package com.iourest.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity(name = "Users")
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
public class Users implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Size(min = 1, max = 100)
    @NotBlank(message = "Name is mandatory")
    private String name;

    @Column(name = "balance", precision = 18, scale = 2)
    private BigDecimal balance = BigDecimal.ZERO;

    @OneToMany(mappedBy = "borrower", cascade = {CascadeType.ALL}, orphanRemoval = true)
    @JsonManagedReference
    private List<Entries> owes = new ArrayList<>();

    @OneToMany(mappedBy = "lender", cascade = {CascadeType.ALL}, orphanRemoval = true)
    @JsonManagedReference
    private List<Entries> owedBy = new ArrayList<>();



}
