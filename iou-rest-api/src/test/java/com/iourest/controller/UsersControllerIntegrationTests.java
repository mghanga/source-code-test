package com.iourest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iourest.controllers.UsersController;
import com.iourest.entities.Users;
import com.iourest.repository.UsersRepository;
import com.iourest.vm.IouVm;
import com.iourest.vm.UserVm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class UsersControllerIntegrationTests {

    private MockMvc restMockMvc;

    @Autowired private UsersController usersController;
    @Autowired private ObjectMapper objectMapper;
    @Autowired private UsersRepository usersRepository;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.restMockMvc = MockMvcBuilders.standaloneSetup( usersController )
                .build();

        Users user1 = new Users();
        user1.setName("Bob");
        usersRepository.save( user1 );

        Users user2 = new Users();
        user2.setName("James");
        usersRepository.save( user2 );
    }

    @Test
    @Transactional
    public void test_creating_new_user_returns_ok() throws Exception {
        UserVm request = new UserVm();
        request.setName("Bernard");

        restMockMvc.perform(post("/add")
                .contentType( MediaType.APPLICATION_JSON )
                .content( objectMapper.writeValueAsBytes( request ) ))
                .andExpect(status().isOk() );
    }

    @Test
    @Transactional
    public void test_creating_existing_user_returns_400() throws Exception {
        UserVm request = new UserVm();
        request.setName("Bob");

        restMockMvc.perform(post("/add")
                .contentType( MediaType.APPLICATION_JSON )
                .content( objectMapper.writeValueAsBytes( request ) ))
                .andExpect(status().isBadRequest() );
    }

    @Test
    @Transactional
    public void test_get_list_of_users_returns_ok() throws Exception {
        restMockMvc.perform(get("/users")
                .queryParam("users", "Bob" )
                .queryParam("users", "James" )
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].name").value(hasItem("Bob")))
                .andExpect(jsonPath("$.[*].balance").value( hasItem( 0 )));
    }


    @Test
    @Transactional
    public void test_create_iou_entry_returns_ok() throws Exception {
        IouVm request = new IouVm();
        request.setBorrower("Bob");
        request.setLender("James");
        request.setAmount( new BigDecimal( 100 ));

        // Post entry
        restMockMvc.perform( post("/iou")
                .accept(MediaType.APPLICATION_JSON)
                .contentType( MediaType.APPLICATION_JSON )
                .content( objectMapper.writeValueAsBytes( request ) ))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void test_create_iou_entry_with_invalid_user_returns_400() throws Exception {
        IouVm request = new IouVm();
        request.setBorrower("Bob");
        request.setLender("John");
        request.setAmount( new BigDecimal( 100 ));

        // Post entry
        restMockMvc.perform( post("/iou")
                .accept(MediaType.APPLICATION_JSON)
                .contentType( MediaType.APPLICATION_JSON )
                .content( objectMapper.writeValueAsBytes( request ) ))
                .andExpect(status().isBadRequest());
    }
}
