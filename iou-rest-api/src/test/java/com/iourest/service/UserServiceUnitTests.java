package com.iourest.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iourest.controllers.UsersController;
import com.iourest.repository.UsersRepository;
import com.iourest.vm.IouVm;
import com.iourest.vm.UserVm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class UserServiceUnitTests {

    @Autowired private UsersRepository usersRepository;
    @Autowired private UsersController usersController;
    @Autowired private UsersService usersService;

    @Autowired private ObjectMapper objectMapper;
    private MockMvc restMockMvc;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.restMockMvc = MockMvcBuilders.standaloneSetup( usersController )
                .build();
    }

    @Test
    public void test_create_user() {
        // Create a new user
        UserVm userVm = new UserVm();
        userVm.setName("Bob");

        JsonNode userObj = (JsonNode)usersService.createUser( userVm );
        assertThat( userObj.get("name").asText() ).isEqualTo("Bob");
        assertThat( userObj.get("balance").asDouble() ).isEqualByComparingTo( 0.0 );
    }

    @Test
    public void test_create_user_with_existing_name_returns_400() {
        UserVm userVm = new UserVm();
        userVm.setName("Bob");

        // Create user
        usersService.createUser( userVm );

        // Attempt to create the same record
        ResponseStatusException ex = Assertions.assertThrows(ResponseStatusException.class, () -> usersService.createUser( userVm ));
        assertThat( ex.getStatus().value() ).isEqualTo( 400 );
        assertThat( ex.getMessage() ).contains("User already exists");
    }

    @Test
    @Transactional
    public void test_get_list_of_users_returns_ok() {
        UserVm userVm = new UserVm();
        userVm.setName("Bob");
        usersService.createUser( userVm );

        UserVm userVm2 = new UserVm();
        userVm2.setName("James");
        usersService.createUser( userVm2 );

        List<JsonNode> rootNode = (List<JsonNode>)usersService.getUsersByNames(Arrays.asList("Bob", "James") );
        assertThat( rootNode.get(0).get("name").asText() ).isEqualTo( "Bob" );
        assertThat( rootNode.get(1).get("name").asText() ).isEqualTo( "James" );
    }

    @Test
    @Transactional
    public void test_create_iou_entry_returns_ok() {
        UserVm userVm = new UserVm();
        userVm.setName("Bob");
        usersService.createUser( userVm );

        UserVm userVm2 = new UserVm();
        userVm2.setName("James");
        usersService.createUser( userVm2 );

        IouVm request = new IouVm();
        request.setBorrower("Bob");
        request.setLender("James");
        request.setAmount( new BigDecimal( 100 ));

        List<JsonNode> rootNode = (List<JsonNode>)usersService.createIouEntry( request );

        assertThat( rootNode.get(0).get("name").asText() ).isEqualTo( "Bob" );
        assertThat( rootNode.get(0).get("balance").asDouble() ).isEqualTo(  -100.0 );
        assertThat( rootNode.get(1).get("name").asText() ).isEqualTo( "James" );
        assertThat( rootNode.get(1).get("balance").asDouble() ).isEqualTo(  100.0 );
    }

    @Test
    @Transactional
    public void test_create_iou_entry_with_invalid_user_returns_400()  {
        IouVm request = new IouVm();
        request.setBorrower("Bob");
        request.setLender("James");
        request.setAmount( new BigDecimal( 100 ));

        ResponseStatusException ex = Assertions.assertThrows(ResponseStatusException.class, () -> usersService.createIouEntry( request ));
        assertThat( ex.getStatus().value() ).isEqualTo( 400 );
        assertThat( ex.getMessage() ).contains("The lender is unknown.");

    }
}
