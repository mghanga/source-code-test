public class DartSolution {

    public int score(int coordinateX, int coordinateY){
        int score = 0;

        // Use Euclidean distance style but do away with the square root
        int radiusPositionOfThePoint = (coordinateX * coordinateX) +  (coordinateY * coordinateY);

        switch( radiusPositionOfThePoint ){
            case 1:
                score = 10;
                break;
            case 5:
                score = 5;
                break;
            case 10:
                score = 1;
                break;

                // default case is 0, and which is already initialized in score
        }
        return score;
    }

    public static void main(String[] args){
        DartSolution s = new DartSolution();
        System.out.println( s.score( 0, 10)); // 1
        System.out.println( s.score( 0, 1)); // 10
    }
}
