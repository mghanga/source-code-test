import java.util.Arrays;
import java.util.List;

public class KnapsackSolution {

    public static class Item {
        private int weight;
        private int value;

        public Item() {
        }

        public Item(int weight, int value) {
            this.weight = weight;
            this.value = value;
        }

        public int getWeight() {
            return weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    public int solution(List<Item> items, int knapSackLimit, int noOfItems) {

        // When the number of items or the weight is zero, end prematurely
        if (noOfItems == 0 || knapSackLimit == 0) return 0;

        Item lastItemInTheList = items.get(noOfItems - 1);
        if ( lastItemInTheList.getWeight() > knapSackLimit) {
            return solution(items, knapSackLimit, noOfItems - 1);
        } else {
            return Math.max(
                    lastItemInTheList.getValue() + solution(items, (knapSackLimit - lastItemInTheList.getWeight()), noOfItems - 1),
                    solution(items, knapSackLimit, noOfItems - 1)
            );
        }
    }


    public static void main(String[] args){
        KnapsackSolution s = new KnapsackSolution();
         //[ { "weight": 5, "value": 10 }, { "weight": 4, "value": 40 }, { "weight": 6, "value": 30 }, { "weight": 4, "value": 50 } ]
        List<Item> items = Arrays.asList(
                new Item(4, 50),
                new Item(4, 40),
                new Item(6, 30),
                new Item(5, 10)
        );

        List<Item> items2 = Arrays.asList(
                new Item(2, 5),
                new Item(2, 5),
                new Item(2, 5),
                new Item(10, 21)
        );

        List<Item> items3 = Arrays.asList(
                new Item(2, 20),
                new Item(2, 20),
                new Item(2, 20),
                new Item(2, 20),
                new Item(10, 50)
        );

        System.out.println( s.solution( items, 10, items.size() )); //90
        System.out.println( s.solution( items2, 10, items.size() )); //21
        System.out.println( s.solution( items3, 10, items.size() )); //80
    }

}
