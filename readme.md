## Senior Software Engineer Code Test

### Solutions
* Question 1 - DartSolution.java
* Question 2 - KnapSackSolution.java
* Question 4 - ice and fire web interface ( HTML5, Boostrap & Jquery )
* Question 5 - iou-rest-api ( spring boot maven project)

### Development Stack
* Java 8
* Maven 3.6.3
* Spring Boot 2.3.3.RELEASE
* Jquery
* HTML & Bootstrap v4 latest
