## Ice & Fire Web Service

### Access the web service
On the project root folder, open index.html using your browser.

#### Development Stack
* Jquery 1.11.3
* BooTstrap v4.5.2
* HTML5
* Javacript

#### Limitations
Pagination of content limited to 50 items( if any )

### Improvements
Implementing the same using a frontend library/framework such as reactjs, angularjs
