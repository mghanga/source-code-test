{

    let housesTable= $('.houses-table').DataTable({
        ajax: {
            url: 'https://anapioficeandfire.com/api/houses?page=1&pageSize=50',
            dataSrc: ''
        },
        "columns": [
            {
                "class":          "details-control",
                "orderable":      false,
                "data":           null,
                "defaultContent": ""
            },
            { "data": "name" }
        ],
    });

    let housesTableDetails = ( rowData ) =>{
        console.log('rowData: ', rowData);
        let seats = 'N/A';
        if ( rowData.seats.length > 0 )  seats = rowData.seats.join(', ');

        let titles = 'N/A';
        if ( rowData.titles.length > 0 )  titles = rowData.titles.join(', ');

        let template = `<div class="row">
                            <div class="col-md-5 order-md-2 mb-5">
                                <ul class="list-group mb-3">
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Name</h6>
                                        </div>
                                        <span class="text-muted">${rowData.name}</span>
                                    </li>
                                     <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Region</h6>                                            
                                        </div>
                                        <small class="text-muted">${rowData.region}</small>
                                    </li>
                                     <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Court of Arms</h6>                                            
                                        </div>
                                        <small class="text-muted">${rowData.coatOfArms}</small>
                                    </li>
                                     <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Words</h6>                                            
                                        </div>
                                        <small class="text-muted">${rowData.words}</small>
                                    </li>
                                     <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Seats</h6>                                            
                                        </div>
                                        <small class="text-muted">${seats}</small>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Titles</h6>                                            
                                        </div>
                                        <small class="text-muted">${titles}</small>
                                    </li>
                                </ul>
                            </div>
                        </div>`;

        return template;
    };

    // Array to track the ids of the details displayed rows
    var housesTableDetailRows = [];

    $('.houses-table tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = housesTable.row( tr );
        var idx = $.inArray( tr.attr('id'), housesTableDetailRows );

        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();

            // Remove from the 'open' array
            housesTableDetailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( housesTableDetails( row.data() ) ).show();

            // Add to the 'open' array
            if ( idx === -1 ) {
                housesTableDetailRows.push( tr.attr('id') );
            }
        }
    } );

    // On each draw, loop over the `detailRows` array and show any child rows
    housesTable.on( 'draw', function () {
        $.each( housesTableDetailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );
}
