{
    const urlParams = new URLSearchParams(window.location.search);
    const characterId = urlParams.get('id');

    fetch('https://anapioficeandfire.com/api/characters/' + characterId )
        .then( response => response.json() )
        .then(data =>{
            let tvSeries = 'N/A';
            if ( data.tvSeries.length > 0 )  tvSeries = data.tvSeries.join(', ');

            let aliases = 'N/A';
            if ( data.aliases.length > 0 )  aliases = data.aliases.join(', ');

            document.querySelector('.character-name').innerHTML =  data.name;
            document.querySelector('.gender').innerHTML =  data.gender;
            document.querySelector('.aliases').innerHTML =  aliases;
            document.querySelector('.tv-series').innerHTML =  tvSeries;
            document.querySelector('.played-by').innerHTML =  data.playedBy[0];
            document.querySelector('.culture').innerHTML =  data.culture;
            document.querySelector('.born').innerHTML =  data.born;
            document.querySelector('.died').innerHTML =  data.died;
            document.querySelector('.titles').innerHTML =  data.titles;
        });
}
