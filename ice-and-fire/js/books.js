{

    let booksTable= $('.books-table').DataTable({
        ajax: {
            url: 'https://anapioficeandfire.com/api/books?page=1&pageSize=50',
            dataSrc: ''
        },
        "columns": [
            {
                "class":          "details-control",
                "orderable":      false,
                "data":           null,
                "defaultContent": ""
            },
            { "data": "name" }
        ],
    });

    let booksTableDetails = ( rowData ) =>{
        console.log('rowData: ', rowData);
        let authors = 'N/A';
        if ( rowData.authors.length > 0 )  authors = rowData.authors.join(', ');

        let template = `<div class="row">
                            <div class="col-md-5 order-md-2 mb-5">
                                <ul class="list-group mb-3">
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Name</h6>
                                        </div>
                                        <span class="text-muted">${rowData.name}</span>
                                    </li>
                                     <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">ISBN</h6>                                            
                                        </div>
                                        <small class="text-muted">${rowData.isbn}</small>
                                    </li>
                                     <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Authors</h6>                                            
                                        </div>
                                        <small class="text-muted">${authors}</small>
                                    </li>
                                     <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Pages</h6>                                            
                                        </div>
                                        <small class="text-muted">${rowData.numberOfPages}</small>
                                    </li>
                                     <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Publisher</h6>                                            
                                        </div>
                                        <small class="text-muted">${rowData.publisher}</small>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Country</h6>                                            
                                        </div>
                                        <small class="text-muted">${rowData.country}</small>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Media Type</h6>                                            
                                        </div>
                                        <small class="text-muted">${rowData.mediaType}</small>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Released</h6>                                            
                                        </div>
                                        <small class="text-muted">${rowData.released}</small>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">No of Characters</h6>                                            
                                        </div>
                                        <small class="text-muted">${rowData.characters.length}</small>
                                    </li>
                                </ul>
                            </div>
                        </div>`;

        return template;
    };

    // Array to track the ids of the details displayed rows
    var booksTableDetailRows = [];

    $('.books-table tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = booksTable.row( tr );
        var idx = $.inArray( tr.attr('id'), booksTableDetailRows );

        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();

            // Remove from the 'open' array
            booksTableDetailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( booksTableDetails( row.data() ) ).show();

            // Add to the 'open' array
            if ( idx === -1 ) {
                booksTableDetailRows.push( tr.attr('id') );
            }
        }
    } );

    // On each draw, loop over the `detailRows` array and show any child rows
    booksTable.on( 'draw', function () {
        $.each( booksTableDetailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );
}
