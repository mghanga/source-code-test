{

    const parseNameOrAlias = data =>{
        let nameOrAlias = "N/A";
        if( data.name === "" ) nameOrAlias = data.aliases[0];
        else nameOrAlias = data.name;

        let pathArray = data.url.split('/');
        const characterId =  pathArray[pathArray.length - 1];
        let template = `<a target="_blank" href="character-details.html?id=${characterId}">${nameOrAlias}</a>`;
        return template;
    };

    let characterTable= $('.characters-table').DataTable({
        ajax: {
            url: 'https://anapioficeandfire.com/api/characters?page=1&pageSize=50',
            dataSrc: ''
        },
        "columns": [
            {
                "class":          "details-control",
                "orderable":      false,
                "data":           null,
                "defaultContent": ""
            },
            { "data": "name" }
        ],
        "rowCallback": function( row, data ) {
            $('td:eq(1)', row).html( parseNameOrAlias( data ) );
        },
    });

    let characterTableDetails = ( rowData ) =>{
        console.log('rowData: ', rowData);
        let tvSeries = 'N/A';
        if ( rowData.tvSeries.length > 0 )  tvSeries = rowData.tvSeries.join(', ');

        let template = `<div class="row">
                            <div class="col-md-5 order-md-2 mb-5">
                                <ul class="list-group mb-3">
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Name</h6>
                                        </div>
                                        <span class="text-muted">${rowData.name}</span>
                                    </li>
                                     <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Gender</h6>                                            
                                        </div>
                                        <small class="text-muted">${rowData.gender}</small>
                                    </li>
                                     <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Aliases</h6>                                            
                                        </div>
                                        <small class="text-muted">${rowData.aliases[0]}</small>
                                    </li>
                                     <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">TV Series</h6>                                            
                                        </div>
                                        <small class="text-muted">${tvSeries}</small>
                                    </li>
                                     <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Played By</h6>                                            
                                        </div>
                                        <small class="text-muted">${rowData.playedBy[0]}</small>
                                    </li>
                                </ul>
                            </div>
                        </div>`;

        return template;
    };

    // Array to track the ids of the details displayed rows
    var characterTableDetailRows = [];

    $('.characters-table tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = characterTable.row( tr );
        var idx = $.inArray( tr.attr('id'), characterTableDetailRows );

        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();

            // Remove from the 'open' array
            characterTableDetailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( characterTableDetails( row.data() ) ).show();

            // Add to the 'open' array
            if ( idx === -1 ) {
                characterTableDetailRows.push( tr.attr('id') );
            }
        }
    } );

    // On each draw, loop over the `detailRows` array and show any child rows
    characterTable.on( 'draw', function () {
        $.each( characterTableDetailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );
}
